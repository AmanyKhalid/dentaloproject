import React from 'react';
import '../css/chart.css';
import Model from './model.js';
import ChartNote from './chartNote.js';
import ShowNote from './showNote.js';



export default function Chart2Pat(argument) {
	return(
		<div className="Chart2">
		    <div className="mainChartImg">
				<img src="../image/kidschart.png" alt="kidsChart"/>
			</div>
			{Array.apply(0, Array(20)).map((x,i) => 				
			<img src="../image/Ellipse65.png" alt="tooth"
				className={"tooth"+i} key={i} data-toggle="modal" 
				data-target="#ShowNote"/>
		   
			)}
			<Model 
				  ModelHeader="Show Note"
				  HeaderIcon="fas fa-notes-medical"
				  ModelId="ShowNote" 
				  ModelContent=<ShowNote/>
				/>	
		</div>
	);
}