import React,{Component} from 'react';
import DentistList from '../Api/dentist.json';
import Model from './model.js';
import getAll from '../Api/getAll.js';
import FadeIn from 'react-fade-in';
import DentistProfile from './dentistProfile.js';


export default class DentistLists extends Component{

  state ={
    Dentists:[]
}
  
  
  componentDidMount(){
	     getAll.getAll(DentistList)
	     .then(data=>{
	      this.setState({
	        Dentists:data
	      })
	      console.log(data);
	     });
    }
	

	render(){
		return(
			<div className="DentistList">
				
				<div className="container ">
				<FadeIn delay="100">
				<div className="row headerItem">
					<div className="col-md-2"> Image :</div>
					<div className="col-md-2">Name :</div>
					<div className="col-md-2">Clinic Name :</div>
					<div className="col-md-2">City :</div>
					<div className="col-md-2">Address :</div>
					<div className="col-md-2">PhoneNo :</div>
				</div>
				</FadeIn>
					
					<div className="allDentist">
						{this.state.Dentists.map(Dentist =>
							<div key={Dentist.id}>
							    <FadeIn delay="300">
								<a href={Dentist.id} data-toggle="modal" data-target="#dentistProfile"  data-id={Dentist.id} >
								    <div className="row OneItem" >	
										<div className="col-md-2">
											<img src={Dentist.Image} alt="dector"/>
										</div>
										<div className="col-md-2">{Dentist.FirstName +" "+ Dentist.LastName}</div>
										<div className="col-md-2">{Dentist.ClinicName}</div>
										<div className="col-md-2">{Dentist.City}</div>
										<div className="col-md-2">{Dentist.Address}</div>
										<div className="col-md-2">{Dentist.PhoneNo}</div>
								    </div>			               
								</a>							 
								</FadeIn>
								<Model 
								    ModelHeader="Dentist Profile"
								    HeaderIcon="fas fa-user-md"
								    ModelId="dentistProfile" 
								    ModelContent=<DentistProfile Dentist={Dentist}/>
				                />
							</div>							
						)}						
					</div> 
				</div>
			</div>
		);
	}
}