import React from 'react';
import '../css/chart.css';
import Model from './model.js';
import ChartNote from './chartNote.js';
import ShowNote from './showNote.js';

export default function Chart1() {
	const chart="#showNote";
	const checkColor=()=> {
		if((document.getElementsByTagName("img").style.backgroundColor) =='red'){
			chart="#chartNote"
		}else{
			chart="#showNote"
		}
		return(chart);
	}
	
	return(

		<div className="Chart1">
		    <div className="mainChartImg">
				<img src="../image/olderchart.png" alt="olderChart"/>
			</div>
			{Array.apply(1, Array(32)).map((x,i) => 
			<img src="../image/Ellipse65.png" alt="tooth"
				id={"toothD"+i} key={i} data-toggle="modal"				
				data-target="#chartNote"/>

			)}
					
				<Model 
				  ModelHeader="Chart Note"
				  HeaderIcon="fas fa-notes-medical"
				  ModelId="chartNote"
				  ModelContent=<ChartNote/>
				/>
		    
				<Model 
				  ModelHeader="Show Note"
				  HeaderIcon="fas fa-notes-medical"
				  ModelId="chartNote" 
				  ModelContent=<ShowNote/>
				/>		
		</div>
	);
}