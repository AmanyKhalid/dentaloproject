
import React from 'react';
import Logo from '../components/logo.js';


export default function ProfileHeader(argument) {
	return(
		<div className="dentistProfile">
				<header>
					<div className="row">
						<div className="col-md-3">
						  <Logo />
						</div>
						<div className="offset-md-1 col-md-3">
							<div className="input-group flex-nowrap">
		                       <div className="input-group-prepend">
		                            <span className="input-group-text"
		                             id="addon-wrapping"><i className="fas fa-search"></i></span>
		                        </div>
		                            <input type="text" className="form-control"
		                              placeholder="Search" aria-label="Search"
		                              aria-describedby="addon-wrapping"/>
		                    </div>
						</div>
						<div className="offset-md-2 col-md-3">
						  <button className="btn">Sing Out</button>

						</div>
					</div>
		        </header>
		    </div>
		);
}