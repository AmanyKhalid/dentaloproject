import React,{Component} from 'react';
import ItemList from './ListItems.js';
import DrugsItem from '../Api/drugs.json';
import getAll from '../Api/getAll.js';
import Model from './model.js';
import AddDrugs from './addDrugs.js';
import $ from 'jquery';


export default class DrugsList extends Component{

  state ={
    drugs:[]
}
  
  
  componentDidMount(){
	     getAll.getAll(DrugsItem)
	     .then(data=>{
	      this.setState({
	        drugs:data
	      })
	      console.log(data);
	     });
	      $("#addDrugs").click(function(){
	    	if ( $("#addPatientDiv").is( ":hidden" ) ) {
            $("#addPatientDiv").slideDown("slow");
        }else{
        	$("#addPatientDiv").hide("slow");
        }
        })
    }
	

	render(){
		return(
			<div className="ListItems">
				<ItemList 
				 headerlist="Drugs List"
				 buttonlist="Add Drugs"
				 headerIcon="fas fa-pills"
				 dataTarget="addDrugs"
				/>
				<div id="addPatientDiv">
					<AddDrugs/>
			    </div>
				
				<div className="container ">
					<div className="row rowHeader">
						<div className="col-md-1">No.</div>
						<div className="col-md-2">Drug Name</div>
						<div className="col-md-2">Generic Name</div>
						<div className="col-md-2">Brand Name</div>
						<div className="col-md-2">Cost</div>
					</div>
					<div className="allItems">
						{this.state.drugs.map(drug =>
							<div className="row OneItem" key={drug.id}>	
								<div className="col-md-1">{drug.id}</div>
								<div className="col-md-2">{drug.name}</div>
								<div className="col-md-2">{drug.GenericName}</div>	
								<div className="col-md-2">{drug.BrandName}</div>
								<div className="col-md-2">{drug.Cost}</div>
								<div className="editClass">
								    <button className="btn" 
								        data-toggle="modal"
							            data-target="#editDrugs2">
								        <i className="fas fa-edit"></i>
								    </button>
								    <Model 
										ModelHeader="Edit Drugs"
										HeaderIcon="fas fa-pills"
									    ModelId="editDrugs2"
									    ModelContent=<AddDrugs name={drug.name}
									    GenericName={drug.GenericName}
									    BrandName={drug.BrandName} Cost={drug.Cost}/>
								    />

								    <button className="btn"><i className="fas fa-trash-alt"></i></button>
								</div>	
							</div>
						)}
					</div>

				</div>
			</div>
		);
	}
}