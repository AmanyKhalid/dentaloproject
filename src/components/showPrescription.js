import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class PatientDrugs extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
					   <label htmlFor="GenericName"
					    className="offset-md-1 col-md-3 col-form-label">
					    Generic Name :</label>
					   <div className="col-md-6">
					      <Field name="GenericName" value="newMarket" 
					      className="form-control" id="GenericName"/>
					   </div>
					   <ErrorMessage name = "GenericName"/>
					</div>	

					<div className="form-group row">
					   <label htmlFor="BrandName" 
					    className="offset-md-1 col-md-3 col-form-label">
					    Brand Name : </label>
					   <div className="col-md-6">
					      <Field name="BrandName" value="newBrand"
					      className="form-control" id="BrandName"/>
					   </div>
					   <ErrorMessage name = "BrandName"/>
					</div>  

					<div className="form-group row">
					   <label htmlFor="DosageForm"
					    className="offset-md-1 col-md-3 col-form-label">
					    Dosage Form :</label>
					   <div className="col-md-6">
					      <Field name="DosageForm"  value='newDosageForm'
					      className="form-control" id="DosageForm"/>
					   </div>
					   <ErrorMessage name = "DosageForm"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Quantity"
					    className="offset-md-1 col-md-3 col-form-label">
					    Quantity : </label>
					   <div className="col-md-6">
					      <Field name="Quantity" value="36" 
					      className="form-control" id="Quantity"/>
					   </div>
					   <ErrorMessage name = "Quantity"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="DosageFrequancy"
					    className="offset-md-1 col-md-3 col-form-label">
					    Dosage & Frequancy :</label>
					   <div className="col-md-6">
					      <Field name="DosageFrequancy" value="2 time / days"  
					      className="form-control" id="DosageFrequancy"/>
					   </div>
					   <ErrorMessage name = "DosageFrequancy"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Duration"
					    className="offset-md-1 col-md-3 col-form-label">
					    Duration :</label>
					   <div className="col-md-6">
					      <Field name="Duration" value="8.am / 8.pm"  
					      className="form-control" id="Duration"/>
					   </div>
					   <ErrorMessage name = "Duration"/>
					</div>									          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            GenericName: Yup.string().required(),
            BrandName: Yup.string().required(),
            DosageForm: Yup.string().required(), 
            Quantity: Yup.string().required(),
            DosageFrequancy: Yup.string().required(),
            Duration: Yup.string().required(),           
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{GenericName:"",BrandName:"",
                    DosageForm: "",Quantity:"",DosageFrequancy:"",Duration:""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
