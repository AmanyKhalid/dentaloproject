import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddPayment extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                   	
					<div className="form-group row">
					   <label htmlFor="date"
					    className="offset-md-1 col-md-5 col-form-label">
					    Date of payment :  </label>
					   <div className="col-md-6">
					      <Field name="date" type="date"
					      className="form-control" id="date"/>
					   </div>
					   <ErrorMessage name = "date"/>
					</div>  					

					<div className="form-group row">
					   <label htmlFor="TotalAmounttopay "
					    className="offset-md-1 col-md-5 col-form-label">
					    Total Amount to pay  :</label>
					   <div className="col-md-6">
					      <Field name="TotalAmounttopay" 
					       className="form-control"
                            id="TotalAmounttopay"/>
					   </div>
					   <ErrorMessage name = "TotalAmounttopay"/>
					</div>

                    <div className="form-group row">
                       <label htmlFor="Whatdidpatientpaid "
                        className="offset-md-1 col-md-5 col-form-label">
                        What did patient paid  :</label>
                       <div className="col-md-6">
                          <Field name="Whatdidpatientpaid" 
                           className="form-control"
                           id="Whatdidpatientpaid"/>
                       </div>
                       <ErrorMessage name = "Whatdidpatientpaid"/>
                    </div> 

                    <div className="form-group row">
                       <label htmlFor="Remarks"
                        className="offset-md-1 col-md-5 col-form-label">
                        Remarks :</label>
                       <div className="col-md-6">
                          <Field name="Remarks" 
                           className="form-control"
                           component="textarea" row="4" id="Remarks"/>
                       </div>
                       <ErrorMessage name = "Remarks"/>
                    </div>  	

		            <button className="btn btn-block"
		             type="submit">Save</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            Date: Yup.string().required(),
            TotalAmounttopay: Yup.string().required(),
            Whatdidpatientpaid: Yup.string().required(),
            Remarks: Yup.string().required(),
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{Date:"",TotalAmounttopay:"",
                   Whatdidpatientpaid:"", Remarks: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
