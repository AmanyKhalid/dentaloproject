import React from 'react';
import {getById} from "../Api/getAll";

export default class DentistProfile extends React.Component {
	 
	state={
        DentistPat: {}
    }
    componentDidMount(){
        const id = this.props.Dentist.id;
        getById(parseInt(id))
            .then(DentistPat => {
                this.setState({
                    DentistPat
                });           
            })
    }


		render(){
			const Dentist=this.state.DentistPat;
		return(
			<div className="DentistProfile">
				<div className="row">
					<div className="col-md-6">
					    <form>
					    <img src={Dentist.Image}  className=" mx-auto d-block" />
						   <div className="form-group">
							    <label htmlFor="FirstName">First Name</label>
							    <input  className="form-control" 
							    id="FirstName" value={Dentist.FirstName}/>
						   </div>

						   <div className="form-group">
							    <label htmlFor="LastName">Last Name</label>
							    <input  className="form-control" 
							    id="LastName" value={Dentist.LastName}/>
						   </div>

						    <div className="form-group">
							    <label htmlFor="Degree">Degree</label>
							    <input  className="form-control" 
							    id="Degree" value={Dentist.Degree}/>
						   </div>

						    <div className="form-group">
							    <label htmlFor="Email">Email</label>
							    <input  className="form-control" 
							    id="Email" value={Dentist.Email}/>
						   </div>

						   <div className="form-group">
							    <label htmlFor="PhoneNo">PhoneNo</label>
							    <input  className="form-control" 
							    id="PhoneNo" value={Dentist.PhoneNo}/>
						   </div>
						  
	                    </form>

					</div>
					<div className="col-md-6">
						<form>
						    <img src={Dentist.ClinicImg} alt="ClinicImg" className=" mx-auto d-block"/>
						    <div className="form-group">
							    <label htmlFor="ClinicName">Clinic Name</label>
							    <input  className="form-control"
							     value={Dentist.ClinicName} id="ClinicName"/>
						    </div>

						    <div className="form-group">
							    <label htmlFor="ClinicNo">Clinic No</label>
							    <input  className="form-control"
							     value={Dentist.ClinicNo} id="ClinicNo"/>
						    </div>

						    <div className="form-group">
							    <label htmlFor="City">City</label>
							    <input  className="form-control"
							     value={Dentist.City} id="City"/>
						    </div>

						    <div className="form-group">
							    <label htmlFor="Address">Address</label>
							    <input  className="form-control"
							     value={Dentist.Address} id="Address"/>
						    </div>

						    <div className="form-group">
							    <label htmlFor="Telephone">Telephone</label>
							    <input  className="form-control"
							     value={Dentist.Telephone} id="Telephone"/>
						    </div>


						</form>

					</div>
			</div>
			</div>
		);
	}
}