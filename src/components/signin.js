import React ,{Component} from 'react';
import SignInNav from './sigInNav.js';
import SignUpMain from './signUpNav.js';
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';

export default class SignIn extends Component{

    render(){
		return(
			<BrowserRouter>
				<div>
					<div className="row mainHeader">
						<div className="col-md-6">
						  <Link className="nav-link SignIn" to="/">Sign In</Link>
						</div>
						<div className="col-md-6">
						 <Link className="nav-link Signup" to="/signup">Sign Up</Link>
						</div>
					</div>

					<Route
			            exact
			            path={"/"}
			            render={props => (
			                <SignInNav />
			            )}
			        />
					<Route path="/signup" exact component={SignUpMain} />

	            </div>
			</BrowserRouter>
		);
	}
}