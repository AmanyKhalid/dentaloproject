import React,{Component} from 'react';
import ItemList from './ListItems.js';
import ProceduresItem from '../Api/Procedures.json';
import getAll from '../Api/getAll.js';
import Model from './model.js';
import AddProcedure from './addProcedure.js';
import $ from 'jquery';


export default class ProceduresList extends Component{

  state ={
    Procedures:[]
}
  
  
  componentDidMount(){
	     getAll.getAll(ProceduresItem)
	     .then(data=>{
	      this.setState({
	        Procedures:data
	      })
	      console.log(data);
	     });
	     $("#addProcedure").click(function(){
	    	if ( $("#addPatientDiv").is( ":hidden" ) ) {
            $("#addPatientDiv").slideDown("slow");
        }else{
        	$("#addPatientDiv").hide("slow");
        }
        });
    }
	

	render(){
		return(
			<div className="ListItems">
				<ItemList
				 headerlist="Procedures List"
				 buttonlist="Add Procedure"
				 headerIcon="fas fa-tooth"
				 dataTarget="addProcedure"
				/>

				<div id="addPatientDiv">
					<AddProcedure/>
			    </div>


				
       
				
				<div className="container ">
					<div className="row rowHeader">
						<div className="col-md-1">No.</div>
						<div className="col-md-4">Procedure Name</div>
						
					</div>
					<div className="allItems">
						{this.state.Procedures.map(Procedure =>
							<div className="row OneItem" key={Procedure.id}>	
								<div className="col-md-1">{Procedure.id}</div>
								<div className="col-md-4">{Procedure.name}</div>
								<div className="editClass">
								    <button className="btn" 
								        data-toggle="modal"
							            data-target="#addProcedure2">
								        <i className="fas fa-edit"></i>
								    </button>
								    <Model 
									  ModelHeader="Edit Procedure"
									  HeaderIcon="fas fa-tooth"
									  ModelId="addProcedure2" 
									  ModelContent=<AddProcedure name={Procedure.name}/>
				                    />
								    <button className="btn"><i className="fas fa-trash-alt"></i></button>
								</div>									
							</div>
						)}
					</div>
				</div>
			
			</div>
		);
	}
}