import React from 'react';
export default function ForgetPass(props) {
	return(
		<div className="ForgetPass">
		    <a  data-toggle="modal"
                data-target="#exampleModal">Click Here
            </a>
            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h4 className="modal-title" id="exampleModalLabel">Reset Your Email</h4>
	                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
	                          <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">
	                        <form onSubmit={props.handleSubmit}>
	                          <input type="email" className="form-control"
	                           placeholder="Enter Your Email"/>     
	                        </form>
	                    </div>
	                    <div className="modal-footer">
	                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
	                        <button 
		                        type="button" data-toggle="modal"
	                            data-target="#exampleModal2"
		                        className="btn btn-primary">Send Reset Link
	                        </button>
	                    </div>
	                </div>
	            </div>
            </div>

            <div className="modal fade" id="exampleModal2" tabIndex="-1"
             role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h4 className="modal-title"
	                         id="exampleModalLabel2">Confirmation code</h4>
	                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
	                          <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">
	                        <form onSubmit={props.handleSubmit}>
	                          <input type="number" className="form-control"
	                           placeholder="Enter Confirmation code" />     
	                        </form>
	                    </div>
	                    <div className="modal-footer">
	                        <button type="button"
	                          className="btn btn-secondary"
	                          data-dismiss="modal">Close
	                        </button>
	                        <button 
		                        type="button" data-toggle="modal"
	                            data-target="#exampleModal3"
		                        className="btn btn-primary">Send code 
	                        </button>
	                    </div>
	                </div>
	            </div>
            </div>

            <div className="modal fade" id="exampleModal3" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	            <div className="modal-dialog" role="document">
	                <div className="modal-content">
	                    <div className="modal-header">
	                        <h4 className="modal-title" id="exampleModalLabel3">
	                        Reset Your PassWord</h4>
	                        <button type="button" className="close"
	                         data-dismiss="modal" aria-label="Close">
	                          <span aria-hidden="true">&times;</span>
	                        </button>
	                    </div>
	                    <div className="modal-body">
	                        <form onSubmit={props.handleSubmit}>
	                          <input type="text" className="form-control"
	                           placeholder="Enter New PassWord"/>
	                           <input type="text" className="form-control"
	                           placeholder="Confirm New password"/>     
	                        </form>
	                    </div>
	                    <div className="modal-footer">
	                        <button type="button" className="btn btn-secondary"
	                         data-dismiss="modal">Close</button>
	                        <button 
		                        type="button" 
		                        className="btn btn-primary">Reset PassWord
	                        </button>
	                    </div>
	                </div>
	            </div>
            </div>

		</div>   
    );
}