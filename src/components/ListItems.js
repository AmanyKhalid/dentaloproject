import React from 'react';

export default function ItemList(props){
    const {headerlist,buttonlist,headerIcon,dataTarget}=props;
	return(
			<div className="ItemList">
				<div className="container">
					<h3><i className={headerIcon}></i>{headerlist}</h3>
					<div className="row">
						<div className="col-md-6">
							<div className="input-group flex-nowrap">
		                       <div className="input-group-prepend">
		                            <span className="input-group-text"
		                             id="addon-wrapping"><i className="fas fa-search"></i></span>
		                        </div>
		                            <input type="text" className="form-control"
		                              placeholder="Search" aria-label="Search"
		                              aria-describedby="addon-wrapping"/>
		                    </div>
						</div>
						<div className="col-md-6">
							<button className="btn" id={dataTarget}>{buttonlist}</button>
						</div>
						
				    </div>
			    </div>
		    </div>
		);
}