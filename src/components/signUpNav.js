import React, {Component} from 'react';
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';
import Patient from './Patient.js';
import Dentist from './Dentist';

export default class SignUpNav extends Component {

	

    render(){
		return(
			<BrowserRouter>
				<div className="container SignUpNav">
					<div className="row">
						<div className="col-md-3">
							<p>Register As :</p>
						</div>

						<div className="col-md-3">
							<Link className="nav-link Patient" to="/Patient">Patient</Link>
						</div>

						<div className="col-md-3">
							<Link className="nav-link Patient Dentist " to="/Dentist">Dentist</Link>
						</div>
					</div>
		            <Route path="/Patient" exact
		              render={props => (
			                <Patient/>
			            )}
		            />
				    <Route path="/Dentist" exact component={Dentist} />
				</div>
			</BrowserRouter>
		);
    }
}