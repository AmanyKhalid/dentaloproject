import React from 'react';
import '../css/dentistProfile.css';
export default function ModelForm(props) {
	const {ModelHeader,ModelContent,ModelId,HeaderIcon}=props;

	return(	
		<div className="modal fade" id={ModelId} tabIndex="-2"
		  role="dialog" aria-labelledby="exampleModalLabel"
		  aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
	                    <h4> <i className={HeaderIcon}></i> {ModelHeader}</h4>
	                    <button type="button" className="close"
	                      data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
	                    {ModelContent}
                    </div>
                   
                </div>
            </div>			                   	        
	    </div>
	);
	
}