import React from 'react';
import DentalChart from './allChart.js';
import Model from './model.js';
import MedicalHistory from './MedicalHistory.js';
import AddNote from './addNote.js';
import AddPrescription from './addPrescription.js';
import PatientOppointment from './patientOppointment.js';
import AddPhoto from './addPhoto.js';
import AddPayment from './addPayment.js';
import AddPatient from './addPatient.js';


export default class PatientProfile extends React.Component {

    render(){
	return(
			<div className="PatientProfile">
				<div className="row patientInfo">
					<div className="col-md-4">
						<img src="../image/newsitem_news_2367_346.jpg" alt="patient"/>
					</div>
					<div className="col-md-4">
						<p>Amany Khalid</p>
						<p>25/10/1997</p>
						<p>53698725</p>
						<p>Gaza</p>
					</div>
					<div className="col-md-4">
						<button 
							className="btn edit"
							data-toggle="modal"
							data-target="#editPatient"
							><i className="fas fa-edit"></i>
							 Edit
						</button><br/>
						<button className="btn delete">
						<i className="fas fa-trash-alt"></i> Delete</button>
					</div>
					<Model 
					  ModelHeader="Edit Patient"
					  HeaderIcon="fas fa-edit"
					  ModelId="editPatient" 
					  ModelContent=<AddPatient firstName="Amany"
	                   	lastName="Khalid" email="engamany@gmail.com"
	                   	MiddleName="ahmed" Contact="0599747515"
	                   	BirthDay="25/101/1997" Address="Gaza Alzahra"
	                   	Note="Hello to new Note" Gender="Female"/>
				    />

				</div>
				<div className="addation">
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-file-medical-alt"></i> Medical History</h5>
						</div>
						<div className="col-md-6">
							<button 
								className="btn"
								data-toggle="modal" 
								data-target="#MedicalHistory">
								Add Medical History
							</button>
						</div>
						 <Model 
						  ModelHeader="Medical History"
						  HeaderIcon="fas fa-file-medical-alt"
						  ModelId="MedicalHistory" 
						  ModelContent=<MedicalHistory/>
						 />

					</div>
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-teeth-open"></i> Dental Chart</h5>
						</div>
						<div className="col-md-6">
							<button 
							    className="btn" 
							    data-toggle="modal" 
								data-target="#DentalChart">
								Add Dental Chart
						    </button>
						</div>

						 <Model 
						  ModelHeader="Dental Chart"
						  HeaderIcon="fas fa-teeth-open"
						  ModelId="DentalChart" 
						  ModelContent=<DentalChart/>
						 />
					</div>
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-file-medical"></i> Dental Note</h5>
						</div>
						<div className="col-md-6">
							<button 
								className="btn"
								data-toggle="modal" 
								data-target="#addNote">
								Add Dental Note
							</button>
						</div>
						 <Model 
						  ModelHeader="Note"
						  HeaderIcon="fas fa-teeth-open"
						  ModelId="addNote" 
						  ModelContent=<AddNote/>
						 />
					</div>
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-prescription-bottle-alt"></i> Prescription</h5>
						</div>
						<div className="col-md-6">
							<button 
								className="btn"
								data-target="#addPrescription"
								data-toggle="modal">
								Add Prescription
							</button>
						</div>
						<Model 
						  ModelHeader="Prescription"
						  HeaderIcon="fas fa-prescription-bottle-alt"
						  ModelId="addPrescription" 
						  ModelContent=<AddPrescription/>
						 />
					</div>
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-calendar-check"></i> Appointments</h5>
						</div>
						<div className="col-md-6">
							<button 
								className="btn"
								data-target="#patientOppointment"
							    data-toggle="modal">
								Add Appointments
							</button>
						</div>

						<Model 
						  ModelHeader="Oppointment"
						  HeaderIcon="fas fa-calendar-check"
						  ModelId="patientOppointment" 
						  ModelContent=<PatientOppointment/>
						 />

					</div>
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-image"></i>  Photos</h5>
						</div>
						<div className="col-md-6">
							<button 
								className="btn"
								data-target="#addPhoto"
								data-toggle="modal">
								Add Photos
							</button>
						</div>
						<Model 
						  ModelHeader="X-Ray Photo"
						  HeaderIcon="fas fa-x-ray"
						  ModelId="addPhoto" 
						  ModelContent=<AddPhoto/>
						 />
					</div>
					<div className="row">
						<div className="col-md-6">
							<h5><i className="fas fa-money-bill-alt"></i> Payments </h5>
						</div>
						<div className="col-md-6">
							<button 
								className="btn"
								data-target="#addPayment"
								data-toggle="modal">
								Add Payments 
							</button>
						</div>
						<Model 
						  ModelHeader="Payments"
						  HeaderIcon="fas fa-money-bill-alt"
						  ModelId="addPayment" 
						  ModelContent=<AddPayment/>
						 />
					</div>

				</div>
			</div>
		);
    }
}