import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
import moment from "moment";

export default class AddOppointment extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }
     getCurrentDate=()=>{
		let newDate = new Date()
		let date = newDate.getDate();
		let month = newDate.getMonth() + 1;
		let year = newDate.getFullYear();

	return date +"/" + month +"/"+year;
}

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                   	

					<div className="form-group row">
					   <label htmlFor="date"
					    className="offset-md-1 col-md-3 col-form-label">
					    Date : </label>
					   <div className="col-md-6">
					      <Field name="date" 
					      value={moment().format("DD-MM-YYYY")}
					      className="form-control" id="date"/>
					   </div>
					</div>  

					<div className="form-group row">
					   <label htmlFor="time" 
					   
					    className="offset-md-1 col-md-3 col-form-label">
					    Time :</label>
					   <div className="col-md-6">
					      <Field name="time"
					      value={moment().format("hh:mm:ss")} 
					      className="form-control" id="time"/>
					   </div>
					</div>

					<div className="form-group row">
					   <label htmlFor="Note"
					    className="offset-md-1 col-md-3 col-form-label">
					    Note :</label>
					   <div className="col-md-6">
					      <Field name="note" value="new note to new Patient"
					      className="form-control" id="note"/>
					   </div>
					</div>					
		           	          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            date: Yup.string().required(),
            time: Yup.string().required(),
            note: Yup.string().required(),
            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{time:"",
                    date: "", note: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
