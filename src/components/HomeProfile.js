import React,{Component} from 'react';
import AppointmentList from '../Api/Appointment.json';
import getAll from '../Api/getAll.js';
import Model from './model.js';
import AddOppointment from './addOppointment.js';
import AddPatient from './addPatient.js';
import FadeIn from 'react-fade-in';



export default class HomeProfile extends Component {

	state ={
	    Appointments:[]
	}
  
  
    componentDidMount(){
	     getAll.getAll(AppointmentList)
	     .then(data=>{
	      this.setState({
	        Appointments:data
	      })
	      console.log(data);
	     });
    }
	

	render(){
	return (
		<div className="HomeProfile">
		    <FadeIn delay="100">
			<div>
				<h1>Welcome Dr. Aamny</h1>
			</div>
			</FadeIn>
			<div className="container">
				<div className="row total">
					<div className="col-md-3">
					    <FadeIn delay="200">
						<div className="TotalPat">
	                        <i className="fas fa-users"></i>
	                        <h5>Total Patient : 150</h5>
		                </div>
		                </FadeIn>
					</div>
					<div className="col-md-4">
					    <FadeIn delay="300">
						<div className="TotalPat">
						    <i className="fas fa-notes-medical"></i>
						    <h5>Up-coming Appointment :10</h5>
			            </div>
			            </FadeIn>
					</div>
					<div className="col-md-3">
					    <FadeIn delay="400">
						<div className="TotalPat">
						    <i className="fas fa-user-plus"></i>
						     <button 
						        className="btn" 
						        data-toggle="modal" 
								data-target="#addOppointment">
								Add Appointment
							</button>
			            </div>
			            </FadeIn>
					</div>
					<div className="col-md-2">
					    <FadeIn delay="500">
						<div className="TotalPat">
						    <i className="fas fa-notes-medical"></i>
						    <button 
							    className="btn"
							    data-toggle="modal" 
								data-target="#addPatient">
							    Add Patient
						    </button>
			            </div>
			            </FadeIn>
					</div>
	            </div>
	            </div>
		               
	                <div className="table">
	                    <FadeIn delay="600">
		                <h4>Up-coming Appointments</h4>
		                </FadeIn>
		                <FadeIn delay="700">
		                <div className="row tableHeader">
			                <div className="headList col-md-3">No</div>
			                <div className="headList col-md-3">Patient Name</div>
			                <div className="headList col-md-3">Time</div>
			                <div className="headList col-md-3">Action</div>
		                </div>
		                </FadeIn>
		                <FadeIn delay="800">
		                <div className="allItems">
			                {this.state.Appointments.map(Appointment =>
									<div className="row OneItem" key={Appointment.id}>	
										<div className="col-md-3">-{Appointment.id}</div>
										<div className="col-md-3">{Appointment.PatientNo}</div>
									    <div className="col-md-3">{Appointment.time}</div>
										<div className="col-md-3">{Appointment.note}</div>									
									</div>
							)}
						</div>
						</FadeIn>	               
                </div>
                <Model 
				    ModelId="addOppointment"
				    ModelHeader="Add Oppointment"
					HeaderIcon="far fa-calendar-check" 
					ModelContent=<AddOppointment/>
				/>
				<Model 
					ModelId="addPatient"
					ModelHeader="Patien Information"
					HeaderIcon="fas fa-id-card" 
					ModelContent=<AddPatient/>
				/>					 								               
			</div>
     	);
    }
}