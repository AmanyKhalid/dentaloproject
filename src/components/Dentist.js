import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import * as Yup from 'yup';
class Dentist extends Component {
	
    onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {props.handleSubmit} > 
                <div className="SignUpContent SingnUpDentist">

	                  <div className="fullInput">
		                    <i className="fas fa-user-alt"></i><Field name = "First"
		                      placeholder="First Name" className="Field"/><br/>
		                    <ErrorMessage name = "First" />
	                   </div>

	                   <div className="fullInput">
		                    <i className="fas fa-user-alt"></i><Field name = "Last"
		                      placeholder="Last Name" className="Field"/><br/>
		                    <ErrorMessage name = "Last" />
	                   </div>

	                   <div className="fullInput">
		                    <i className="fas fa-envelope"></i><Field name = "email"
		                      placeholder="example@email.com" className="Field"/><br/>
		                    <ErrorMessage name = "email" />
	                   </div>

	                    <div className="fullInput">
		                    <i className="fas fa-lock"></i><Field name = "password"
		                       placeholder="password" className="Field"/><br/>
		                    <ErrorMessage name = "password" />
	                   </div>

	                    <div className="fullInput">
	                        <i className="fas fa-lock"></i><Field name = "password"
	                           placeholder="Retype password" className="Field"/><br/>
	                       <ErrorMessage name = "password" />
	                   </div>
	                   
	                   <h5>Clinic Information :</h5>

	                    <div className="fullInput">
		                    <i className="fas fa-clinic-medical"></i><Field name = "name"
		                      placeholder="Clinic Name" className="Field"/><br/>
		                    <ErrorMessage name = "name" />
	                   </div>

	                    <div className="fullInput">
		                    <i className="fas fa-phone"></i><Field name = "phone"
		                       placeholder="Clinic Phone " className="Field"/><br/>
		                    <ErrorMessage name = "phone" />
	                   </div>

	                    <div className="fullInput">
		                    <i className="fas fa-location-arrow"></i><Field name = "city"
		                       placeholder="city" className="Field"/><br/>
		                    <ErrorMessage name = "city" />
	                   </div>

		               <div className="btnPage">
			                 <button>Sign Up</button>
			                 <p>Already  Have Account  <a href="#">Sign In</a></p>
			            </div>

                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            email: Yup.string().required(),
            First: Yup.string().required(),
            Last: Yup.string().required(),
            password: Yup.string().required(),
            name: Yup.string().required(),
            city: Yup.string().required(), 
            phone: Yup.string().required(),   
        });
        return schema;
    }

    render() {
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{email: "", password: ""}}
                     onSubmit={this.onSubmit }
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            )
        }
    }
    export default Dentist;