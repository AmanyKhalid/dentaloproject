import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddOppointment extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                   	

					<div className="form-group row">
					   <label htmlFor="date"
					    className="offset-md-1 col-md-3 col-form-label">
					    Date : </label>
					   <div className="col-md-6">
					      <Field name="date" type="date"
					      className="form-control" id="date"/>
					   </div>
					   <ErrorMessage name = "date"/>
					</div>  

					<div className="form-group row">
					   <label htmlFor="time"
					    className="offset-md-1 col-md-3 col-form-label">
					    Time :</label>
					   <div className="col-md-6">
					      <Field name="time" type="time" 
					      className="form-control" id="time"/>
					   </div>
					   <ErrorMessage name = "time"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Note"
					    className="offset-md-1 col-md-3 col-form-label">
					    Note :</label>
					   <div className="col-md-6">
					      <Field name="note" 
					      className="form-control" id="note"/>
					   </div>
					   <ErrorMessage name = "note"/>
					</div>					
		            <button className="btn btn-block"
		             type="submit">Add Oppointment</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            date: Yup.string().required(),
            time: Yup.string().required(),
            note: Yup.string().required(),
            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{time:"",
                    date: "", note: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
