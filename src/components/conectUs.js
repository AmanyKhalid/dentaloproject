import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
import '../css/services.css';

export default class ConectUs extends Component {

    onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
    	const fn=<p> First Name</p>
        return(
            <form onSubmit = {this.handleSubmit} className="ConectUs">
	            <div className="container">
					<div className="row">
						<div className="col-md-6">
							<Field name = "First"
	                          placeholder="First Name" className="Field form-control"/><br/>
	                          <ErrorMessage name = "First" />
						</div>
						<div className=" col-md-6">
							<Field name = "Last"
		                          placeholder="Last Name" className="Field form-control"/><br/>
		                          <ErrorMessage name = "Last" />
						</div>
					</div>
				</div>
				<div className="container">
					<div className="row">
						<div className="col-md-6">
							<Field name = "Phone"
	                          placeholder="Phone Number" className="Field form-control"/><br/>
	                          <ErrorMessage name = "Phone" />
						</div>
						<div className=" col-md-6">
							<Field name = "email"
		                          placeholder="Email" className="Field form-control"/><br/>
		                          <ErrorMessage name = "email" />
						</div>
					</div>
				</div>
				<div className="container">
					<Field name = "textarea" 
	                    placeholder="Massage Here" className="Field form-control textarea"/> <br/>
	                    <ErrorMessage name = "massage" rows="3" />
				</div>
				<div className="container">
					<button className="btn btn-block">Conect</button>
				</div> 
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            email: Yup.string().required(),
            First: Yup.string().required(),
            Last: Yup.string().required(),
            phone: Yup.string().required(),
            massage: Yup.string().required(),  
        });
        return schema;
    }
    render(){
		return(
			<div className="ConectUs">
				<h3>Get in touch with Us!</h3>
				<Formik initialValues ={{firstName:"",lastName:"",
                    phone: "", email: ""}}
                    onSubmit={this.onSubmit}
                    render  ={this.form}
                    validationSchema = {this.schema()}
                /> 
		    </div>
	    );
	}
}