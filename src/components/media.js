import React from 'react';
export default function Media(props) {
	const {mediaIcon,MediaBody}=props;

	return(
		<div className="media">
			<i className={mediaIcon + " mr-1"}></i>
			<div className="media-body">
				<h5>{MediaBody}</h5>
			</div>
		</div>
	)
}