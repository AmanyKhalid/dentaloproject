import React from 'react';
import '../css/chart.css';
import Model from './model.js';
import ChartNote from './chartNote.js';
import ShowNote from './showNote.js';

export default function ChartPat() {
	
	return(

		<div className="Chart1">
		    <div className="mainChartImg">
				<img src="../image/olderchart.png" alt="olderChart"/>
			</div>
			{Array.apply(1, Array(32)).map((x,i) => 
			<img src="../image/Ellipse65.png" alt="tooth"
				id={"toothD"+i} key={i} data-toggle="modal"				
				data-target="#showNote"/>

			)}
		    
				<Model 
				  ModelHeader="Show Note"
				  HeaderIcon="fas fa-notes-medical"
				  ModelId="showNote" 
				  ModelContent=<ShowNote/>
				/>		
		</div>
	);
}