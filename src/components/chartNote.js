import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddChartNote extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
					   <label htmlFor="Note Date"
					    className="offset-md-1 col-md-3 col-form-label">
					    Note Date :</label>
					   <div className="col-md-6">
					      <Field name="NoteDate" type="date" 
					      className="form-control" id="NoteDate"/>
					   </div>
					   <ErrorMessage name ="NoteDate"/>
					</div>	

					<div className="form-group row">
					   <label htmlFor="Procedure"
					    className="offset-md-1 col-md-3 col-form-label">
					    Procedure :  </label>
					   <div className="col-md-6">
					      <Field name="Procedure"
					      className="form-control" id="Procedure"/>
					   </div>
					   <ErrorMessage name ="Procedure"/>
					</div>  

					<div className="form-group row">
					   <label htmlFor="Teethcolor"
					    className="offset-md-1 col-md-3 col-form-label">
					    Teeth color :</label>
					   <div className="col-md-6">
						    <Field name="Teethcolor"
		                      type ="radio" value = "red" />
		                      <i className="fas fa-square-full red"></i> 

		                    <Field name="Teethcolor"
	                         type ="radio" value = "green" />
	                         <i className="fas fa-square-full green"></i> 
	                          <Field name="Teethcolor"
		                      type ="radio" value = "blue" />
		                      <i className="fas fa-square-full blue"></i> 

		                    <Field name="Teethcolor"
	                         type ="radio" value = "Purple" />
	                         <i className="fas fa-square-full Purple"></i> 

	                         <Field name="Teethcolor"
	                         type ="radio" value = "yellow" />
	                         <i className="fas fa-square-full yellow"></i>

					   </div>
					   <ErrorMessage name ="Teethcolor"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Note"
					    className="offset-md-1 col-md-3 col-form-label">
					    Note :</label>
					   <div className="col-md-6">
					      <Field name="note" 
					      className="form-control" id="note"/>
					   </div>
					   <ErrorMessage name ="note"/>
					</div>					
		            <button className="btn btn-block"
		             type="submit">Save</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            NoteDate: Yup.string().required(),
            Procedure: Yup.string().required(),
            Teethcolor: Yup.string().required(),
            note: Yup.string().required(),
            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{PatientName:"",time:"",
                    date: "", note: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
