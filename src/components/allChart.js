import React from 'react';
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';
import AdultChart from './chart.js';
import KidsChart from './chart2.js';
import '../css/chart.css';
export default function AllChart(argument) {
	return(
		<BrowserRouter>
			<div className="row">
				<div className="col-md-6">
				  <NavLink className="nav-link" to="/kids">
				    Kids
				  </NavLink>
				</div>
				<div className="col-md-6">
				   <NavLink className="nav-link" to="/Adult">
				     Adult
				   </NavLink>
				</div>
			</div>
			<Route path="/Kids" exact component={KidsChart} />
			<Route path="/Adult" exact component={AdultChart} />
			<button className="btn done">Done</button>
		</BrowserRouter>


    );
}