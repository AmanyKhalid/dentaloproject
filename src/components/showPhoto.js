import React,{Component} from 'react';
import Photos from '../Api/photo.json';
import getAll from '../Api/getAll.js';

export default class  ShowPhotoextends extends Component{

	state ={
	    photos:[]
	}
	   
  
    componentDidMount(){
	    getAll.getAll(Photos)
	    .then(data=>{
	    this.setState({
	        photos:data
	    })
	      console.log(data);
	    });
    }
    render(){
		return(
			<div className="ShowPhoto">						
				<nav>
				    <div className="nav nav-tabs active" id="nav-tab" role="tablist"> 
						<a className="nav-item nav-link active" id="nav-one-tab" 
						data-toggle="tab" href="#nav-one" role="tab"
						aria-controls="nav-one" aria-selected="true">First Photo</a>
						<a className="nav-item nav-link" id="nav-tow-tab" 
						data-toggle="tab" href="#nav-tow" role="tab"
						aria-controls="nav-tow" aria-selected="false">Second Photo</a>
						<a className="nav-item nav-link" id="nav-three-tab" 
						data-toggle="tab" href="#nav-three" role="tab"
						aria-controls="nav-three" aria-selected="false">Thrid Photo</a>
				    </div>
		        </nav>

				<div className="tab-content" id="nav-tabContent">
					<div className="tab-pane fade show active" id="nav-one" 
					role="tabpanel" aria-labelledby="nav-one-tab">
					   <img src="../image/x-ray3.jpg" alt="photo1"/><br/>
					   <p> Note Photo One to help you manage your 
				           patient information. Instead of using
				           index cards, we have created an app 
				           that you can use on different devices
				        </p>
			        </div>
			        <div className="tab-pane fade show " id="nav-tow" 
					role="tabpanel" aria-labelledby="nav-tow-tab">
					   <img src="../image/x-ray3.jpg" alt="photo1"/><br/>
					    <p> Note Photo Tow to help you manage your 
				           patient information. Instead of using
				           index cards, we have created an app 
				           that you can use on different devices
				        </p>				
			        </div>
			        <div className="tab-pane fade show " id="nav-three" 
					role="tabpanel" aria-labelledby="nav-three-tab">
					   <img src="../image/x-ray3.jpg" alt="photo1"/><br/>
					    <p> Note Photo Three to help you manage your 
				           patient information. Instead of using
				           index cards, we have created an app 
				           that you can use on different devices
				        </p>					
			        </div>		
				</div>
		    </div>	
	    );
	}
}