import React from 'react';
import '../css/signin.css';
import Logo from '../components/logo.js';
import ServicesPage from '../components/Services.js';

import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';

export default function Header() {
	return (
		<BrowserRouter>
			<nav className="navbar navbar-expand-lg row ">
				  <a className="navbar-brand  col-md-3" href="#"><Logo /></a>
				  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span className="navbar-toggler-icon"><i className="fas fa-list-ul"></i></span>
				  </button>
				 <div className="collapse navbar-collapse container" id="navbarNav">
				    <ul className="navbar-nav offset-md-1 col-md-5">
				      <li className="nav-item active">
				        <Link className="nav-link" to="/home">
				        Home <span className="sr-only">(current)</span></Link>
				      </li>
				      <li className="nav-item">
				        <Link className="nav-link" to="/Services">Services</Link>
				      </li>
				      <li className="nav-item">
				        <Link className="nav-link" to="/About">About Us</Link>
				      </li>
				      <li className="nav-item">
				        <Link className="nav-link" to="/ConectUs">Conect Us</Link>
				      </li>
				    </ul>
				    <div className=" col-md-3">
					    <button className="btn">Sing In</button>
					    <button className="btn">Sing up</button>
				    </div>
				 
				</div>
			</nav>
			<Route path="/Services" exact component={ServicesPage} />

		</BrowserRouter>
	);
}
