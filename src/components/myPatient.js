import React,{Component} from 'react';
import {BrowserRouter,Route,Link,NavLink,Switch,useHistory} from 'react-router-dom';
import ItemList from './ListItems.js';
import PatientList from '../Api/patient.json';
import getAll from '../Api/getAll.js';
import PatientProfile from './PatientProfile.js';
import Model from './model.js';
import AddPatient from './addPatient.js';
import $ from 'jquery';

export default class PatientListClass extends Component{

	state ={
	    Patients:[]
	}
	   
  
    componentDidMount(){
	    getAll.getAll(PatientList)
	    .then(data=>{
	    this.setState({
	        Patients:data
	    })
	      console.log(data);
	    });

	    $("#addPaient").click(function(){
	    	if ( $("#addPatientDiv").is( ":hidden" ) ) {
            $("#addPatientDiv").slideDown("slow");
        }else{
        	$("#addPatientDiv").hide("slow");
        }
        });
    }
	

	render(){
		return(
			<BrowserRouter>
				<div className="ListItems">
					<ItemList 
					 headerlist="Patients List"
					 buttonlist="Add Patients" 
					 headerIcon="fas fa-user-injured"
					 dataTarget="addPaient"
					 />
					<div id="addPatientDiv">
					<AddPatient/>
					</div>
					 										
					<div className="container addPatient">
						<div className="row rowHeader">
							<div className="col-md-1">No.</div>
							<div className="col-md-2">Patient Name</div>
							<div className="col-md-2">Note</div>
							<div className="col-md-2">Contact-No</div>
							<div className="col-md-2">Appointment</div>
							<div className="col-md-2">Payment</div>						
						</div>
						<div className="allItems">

							{this.state.Patients.map(Patient =>
								Patient.paitentInfo.map(paitentInfo1=>																
								<a  data-toggle="modal" 
								    data-target="#addPatient"
								    key={paitentInfo1.id}
								    href={"/"+paitentInfo1.id}>
									<div className="row OneItem">	
										<div className="col-md-1">{paitentInfo1.id}</div>
										<div className="col-md-2">{paitentInfo1.firstName+" "}
										{paitentInfo1.lastName}</div>
										<div className="col-md-2">{paitentInfo1.Nots}</div>
										<div className="col-md-2">{paitentInfo1.ContactNo}</div>
										<div className="col-md-2">{paitentInfo1.Appointments}</div>
										<div className="col-md-2">{paitentInfo1.Payments}</div>														
							        </div>
						        </a>
						       						       
							))}
							 
							<Model 
								ModelId="addPatient"
								ModelHeader="Patien Information"
								HeaderIcon="fas fa-id-card" 
								ModelContent=<PatientProfile/>
					        />
						</div>
						<a name="add"></a>
					
															 								                    	                                     					
					</div>
				</div>			
					
			
														
			</BrowserRouter>
		);
	}
}