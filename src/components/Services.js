import React from 'react';
import '../css/services.css';
export default  function ServicesPage(argument) {
	return(
		<div  className="servicesPage">
			<img src="./image/Path 2143.png"/>
		    <div className="servicesHeader">
				<h3>Dentalo Services</h3>
				<p>Designed with ease of use in mind, you will be up and running 
		           with Dentalo  in a few minutes! We have included the services that 
		           you need the most:
		        </p>
	        </div>
	        <div className="container">
	            <div className="row">
		            <div className="col-md-4">
			           <p className="titleSer"><i className="fas fa-address-card"></i>PATIENT INFORMATION</p>
			           <p>Keep track of your patient information.</p>
		            </div>
		            <div className="col-md-4">
			           <p className="titleSer"><i className="fas fa-camera"></i>ATTACH IMAGE</p><br/>
			           <p>Attach images to your patient records.</p>
		            </div>
		            <div className="col-md-3">
			           <p className="titleSer"><i className="fas fa-sms"></i>SEND SMS</p><br/>
			           <p>send sms to your patients in one click.</p>
		            </div>
	            </div>
	            <div className="row">
		            <div className="col-md-4">
			           <p className="titleSer"><i className="fas fa-money-bill"></i>PAYMENT TRANSACTIONS</p>
			           <p>Keep track of your patient's payments.</p>
		            </div>
		            <div className="col-md-4">
			           <p className="titleSer"><i className="fas fa-calendar-check"></i> APPOINTMENTS</p><br/>
			           <p>Keep track of your appointments.</p>
		            </div>
		            <div className="col-md-3">
			           <p className="titleSer"><i className="fas fa-tooth"></i> CHARTS</p><br/>
			           <p>Dental chart that is very easy to use.</p>
		            </div>
	            </div>
	        </div>
	        <img src="./image/Path 2143.png"/>
	    </div>
    );
}