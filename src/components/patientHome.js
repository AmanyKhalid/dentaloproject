import React from 'react';
import Media from './media.js';
import '../css/patientProfile.css';
import FadeIn from 'react-fade-in';
import Model from './model.js';
import ContactUs from './conectUs.js';
import DentalChart from './allChartPat.js';
import NotePatient from './notePat.js';
import ShowPrescription from './showPrescription.js';
import ShowOppointment from './showOppointment.js';
import ShowPayment from './showPayment.js';
import ShowPhoto from './showPhoto.js';

export default function PatientHome() {
	return(
		<div className="container">	
		    <div className="row">
		        <div className="col-md-6">
				    <FadeIn delay="100">
					<h3>Dental Details </h3>
					</FadeIn>
				</div>
				<div className="col-md-6">
				    <FadeIn delay="150">
					<button 
					   className="btn" 
					   data-toggle="modal" 
					   data-target="#ContactUs">
					   Contact us
					</button>

					</FadeIn>
					<Model 
						ModelId="ContactUs"
						ModelHeader="Contact Us"
						HeaderIcon="fas fa-file-signature" 
						ModelContent=<ContactUs/>
			        />
				</div>
			</div>
			<hr/>				
			<div className="row">
				<div className="offset-md-2 col-md-4" >
					<FadeIn delay="200">
					<a data-toggle="modal" data-target="#MedicalHistory">
					     <Media mediaIcon="fas fa-file-medical-alt"
						 MediaBody="Medical History" />
					 </a>
					 </FadeIn>
					<Model 
						ModelId="MedicalHistory"
						ModelHeader="Medical History"
						HeaderIcon="fas fa-file-medical-alt" 
						ModelContent=<div className="history">
							<p> Lorem ipsum dolor sit amet,<br/>
							    consectetur adipisicing elit. Quod deserunt quo labore,<br/>
							    amet sapiente. Nostrum quo asperiores fugit voluptas corporis minus cumque,<br/>
							   aut corrupti sint sunt nam porro, animi incidunt.
							</p> 
						</div>
		            />
				</div>
				<div className="offset-md-1 col-md-4" >
				    <FadeIn delay="300">
					<a data-toggle="modal" data-target="#DentalChart">
						<Media mediaIcon="fas fa-teeth-open"
						 MediaBody="Dental Chart" />
					</a>
					</FadeIn>
					<Model 
					    ModelHeader="Dental Chart"
					    HeaderIcon="fas fa-teeth-open"
					    ModelId="DentalChart" 
					    ModelContent=<DentalChart/>
			        />
				</div>
			</div>

			<div className="row">
				<div className="col-md-4">
				    <FadeIn delay="400">
				    <a data-toggle="modal" data-target="#NotePat">
						<Media mediaIcon="fas fa-file-medical"
						 MediaBody="Dental Note" />
					</a>
					</FadeIn>
					<Model 
					    ModelHeader="Dental Note"
					    HeaderIcon="fas fa-file-medical"
					    ModelId="NotePat" 
					    ModelContent=<NotePatient/>
			        />
				</div>
				<div className="col-md-4">
				    <FadeIn delay="500">
					<a data-toggle="modal" data-target="#showPrescription">
						<Media mediaIcon="fas fa-prescription-bottle-alt"
						 MediaBody="Prescription" />
					</a>
					</FadeIn>
					<Model 
					    ModelHeader="Prescription"
					    HeaderIcon="fas fa-prescription-bottle-alt"
					    ModelId="showPrescription" 
					    ModelContent=<ShowPrescription/>
			        />
				</div>
				<div className="col-md-4">
				    <FadeIn delay="600">
				    <a data-toggle="modal" data-target="#showOppointment">
						<Media mediaIcon="fas fa-calendar-check"
						 MediaBody="Appointments" />
					</a>
					</FadeIn>
					<Model 
					    ModelHeader="Appointments"
					    HeaderIcon="fas fa-calendar-check"
					    ModelId="showOppointment" 
					    ModelContent=<ShowOppointment/>
			        />
				</div>
			</div>

			<div className="row">
				<div className="offset-md-2 col-md-4">
				    <FadeIn delay="700">
				    <a data-toggle="modal" data-target="#showPhoto">
						<Media mediaIcon="fas fa-image"
						 MediaBody="Photos" />
					</a>
					</FadeIn>
					<Model 
					    ModelHeader="Photos"
					    HeaderIcon="fas fa-image"
					    ModelId="showPhoto" 
					    ModelContent=<ShowPhoto/>
			        />
				</div>
				<div className="offset-md-1 col-md-4">
				    <FadeIn delay="800">
				    <a data-toggle="modal" data-target="#showPayment">
						<Media mediaIcon="fas fa-money-bill-alt"
						 MediaBody="Payments" />
				    </a>
					</FadeIn>
					<Model 
					    ModelHeader="Payments"
					    HeaderIcon="fas fa-money-bill-alt"
					    ModelId="showPayment" 
					    ModelContent=<ShowPayment/>
			        />
				</div>
	        </div>
	    </div>
	);
}