import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class MedicalHistory extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
					   <label htmlFor="MedicalHistory"
					    className="offset-md-1 col-md-4 col-form-label">
					    Medical History  :</label>
					   <div className="col-md-7">
					      <Field name="MedicalHistory" component="textarea" rows="6"
					      className="form-control" id="MedicalHistory"/>
					   </div>
					   <ErrorMessage name = "MedicalHistory"/>
					</div>	

					
									
		            <button className="btn btn-block"
		             type="submit">Save</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            MedicalHistory: Yup.string().required(),                        
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik 
                     initialValues ={{MedicalHistory:""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
