import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddProcedure extends Component {

	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
					   <label htmlFor="procedureName"
					    className="offset-md-1 col-md-3 col-form-label">
					    Procedure Name :</label>
					   <div className="col-md-6">
					      <Field name="procedureName" 
					      className="form-control" id="procedureName"/>
					   </div>
					   <ErrorMessage name = "procedureName"/>
					</div>	

					
									
		            <button className="btn btn-block"
		             type="submit">Add Procedure</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            procedureName: Yup.string().required(),                        
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik 
                     initialValues ={{procedureName:this.props.name}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
