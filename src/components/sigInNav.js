import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import ForgetPass from '../components/forgetPass';
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';
import * as Yup from 'yup';
class SignInNav extends Component {

    onSubmit = (values) => {
      this.props.history.push("/dentistProfile");
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <h4>Start Your Session Now</h4>
                <div className="FormContent">
                    <i className="fas fa-envelope icon"></i>
                    <Field name = "email" placeholder="example@mail.com"
                     className="Field" /> <br/> 
                    <ErrorMessage name = "email" /> <br/>
                    <i className="fas fa-lock icon"></i>
                    <Field name = "password" type = "password"
                       className="Field" /> <br/> 
                    <ErrorMessage name = "password" / > < br / > 
                    <button className="btn btn-primary"
                     type = "submit" > Sign In </button>

                </div>
            </form>

        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            password: Yup.string().required(),  
        });
        return schema;
    }

    render() {
            return ( 
                <div className = "formikSign" >
                   <Formik initialValues ={{email: "", password: ""}}
                     onSubmit={this.onSubmit }
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                    <div className="forPass">
                    <p className="pFor">Forget Password ?  </p>
                    <ForgetPass/>
                    </div>
                < /div>
            )
        }
    }
    export default SignInNav;