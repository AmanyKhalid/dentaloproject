import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddDrugs extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
	                <div className="form-group row">
					    <label htmlFor="name"
					    className="offset-md-1 col-md-3 col-form-label">
					     Name :</label>
					    <div className="col-md-6">
					      <Field name="name" 
					      className="form-control" id="name" />
					    </div>
					   <ErrorMessage name = "name"/>
					</div>
                    <div className="form-group row">
					   <label htmlFor="GenericName"
					    className="offset-md-1 col-md-3 col-form-label">
					    Generic Name :</label>
					   <div className="col-md-6">
					      <Field name="GenericName" 
					      className="form-control" id="GenericName"/>
					   </div>
					   <ErrorMessage name = "GenericName"/>
					</div>	

					<div className="form-group row">
					   <label htmlFor="BrandName"
					    className="offset-md-1 col-md-3 col-form-label">
					    Brand Name : </label>
					   <div className="col-md-6">
					      <Field name="BrandName"
					      className="form-control" id="BrandName"/>
					   </div>
					   <ErrorMessage name = "BrandName"/>
					</div>  

					<div className="form-group row">
					   <label htmlFor="Cost"
					    className="offset-md-1 col-md-3 col-form-label">
					    Cost :</label>
					   <div className="col-md-6">
					      <Field name="Cost"  
					      className="form-control" id="Cost"/>
					   </div>
					   <ErrorMessage name = "Cost"/>
					</div>

									
		            <button className="btn btn-block"
		             type="submit">Add Drugs</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            GenericName: Yup.string().required(),
            BrandName: Yup.string().required(),
            Cost: Yup.string().required(),            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{name:this.props.name,GenericName:this.props.GenericName,
                   	BrandName:this.props.BrandName,
                    Cost: this.props.Cost}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
