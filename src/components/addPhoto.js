import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddPhoto extends Component {
	 onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                   	
					<div className="form-group row">
					   <label htmlFor="Photo"
					    className="offset-md-1 col-md-3 col-form-label">
					    X-ray Photo : </label>
					   <div className="col-md-6">
					      <Field name="Photo" type="file"
					      className="form-control" id="Photo"/>
					   </div>
					   <ErrorMessage name = "Photo"/>
					</div>  					

					<div className="form-group row">
					   <label htmlFor="Note"
					    className="offset-md-1 col-md-3 col-form-label">
					    Note :</label>
					   <div className="col-md-6">
					      <Field name="note" 
					      className="form-control" component="textarea" row="4" id="note"/>
					   </div>
					   <ErrorMessage name = "note"/>
					</div>					
		            <button className="btn btn-block"
		             type="submit">Save</button>		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            Photo: Yup.string().required(),
            note: Yup.string().required(),
            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{Photo:"", note: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
