import React from 'react';
import '../css/services.css';
export default function AboutUs() {
	return(
		<div className="aboutUs">
			<h1>DESIGNED TO KEEP THINGS SIMPLE</h1>
			<p>We want to help you manage your 
	           patient information. Instead of using
	           index cards, we have created an app 
	           that you can use on different devices
	        </p>
		</div>
    );
}