import React,{Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import * as Yup from 'yup';

export default class EditProfile extends Component{

	onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {props.handleSubmit} > 
                <div className="row">
					<div className="col-md-5 ">
						<h4><i className="fas fa-address-card"></i>My Information</h4>
						<div className="divImg">
						<img src="../image/doctor.jpg" alt="doctor"/><br/>
                        <input type="file" name="file" id="file" className="inputfile" />
                        <label for="file"><i className="fas fa-camera"></i></label>						</div>

						<label>First Name:</label>
						<Field name = "First" className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "First"/>

			            <label>Last Name:</label>
						<Field name = "Last"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "Last"/>

			            <label>Degree:</label>
						<Field name = "Degree"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "Degree"/>

			            <label>Email:</label>
						<Field name = "email"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "email"/>

			            <label>Phone Number:</label>
						<Field name = "PhoneNo"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "PhoneNo"/>
			        </div>
					<div className="offset-md-1 col-md-5">
						<h4><i className="fas fa-clinic-medical"></i>Clinic Information</h4>
                        <div className="divImg">
						<img src="../image/heart-stethoscope-4285ld.png" alt="doctor"/><br/>
						<input type="file" name="file" id="file" className="inputfile" />
                        <label for="file"><i className="fas fa-camera"></i></label>
						</div>

						<label>Clinic Name:</label>
						<Field name = "ClinicName"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "ClinicName"/>

			            <label>Clinic No:</label>
						<Field name = "ClinicNo" className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "ClinicNo"/>

			            <label>City:</label>
						<Field name = "City"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "City"/>

			            <label>Address:</label>
						<Field name = "Address"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "Address"/>

			            <label>Telephone:</label>
						<Field name = "Telephone"  className="Field form-control-plaintext"/><br/>
			            <ErrorMessage name = "Telephone"/>
					</div>
				</div>
				<button className="btn btn-block" type="onSubmit">Save Change</button>
			</form>
        );
    }
    schema = () => {
        const schema = Yup.object().shape({
            email: Yup.string().required(),
            First: Yup.string().required(),
            Last: Yup.string().required(),
            Degree: Yup.string().required(),
            ClinicName: Yup.string().required(),
            ClinicNo: Yup.string().required(),
            City: Yup.string().required(),
            Address: Yup.string().required(),
            PhoneNo: Yup.string().required(),
            Telephone: Yup.string().required()    
        });
        return schema;
    }

    render(){
		return (
			<div className="EditProfile">
				<div className="container">
					<h3><i className="fas fa-user-md"></i>Profile</h3>

					<Formik 
				        initialValues ={{email: "engamany@gmail.com", First: "Ahmed",
				        Last:"khalid",Degree:"PH.p",PhoneNo:"0599747515",ClinicName:"dentalo",ClinicNo:"123",
				        City:"Gaza",Address:"Alwada Str.",Telephone:"08-2072540"}}
	                    onSubmit={this.onSubmit}
	                    render  ={this.form}
	                    validationSchema = {this.schema()}
	                /> 
			    </div>
			</div>
		);
    }
}