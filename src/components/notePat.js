import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
import moment from 'moment';

export default class NotePatient extends Component {
     onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
                       <label htmlFor="Note Date"
                        className="offset-md-1 col-md-3 col-form-label">
                        Note Date :</label>
                       <div className="col-md-6">
                          <Field name="NoteDate"  
                          className="form-control" value={moment().format("DD-MM-YYYY hh:mm:ss")} id="NoteDate"/>
                            
                       </div>
                       <ErrorMessage name ="NoteDate"/>
                    </div>  

                    <div className="form-group row">
                       <label htmlFor="Procedure"
                        className="offset-md-1 col-md-3 col-form-label">
                        Procedure :  </label>
                       <div className="col-md-6">
                          <Field name="Procedure" value="cleaning"
                          className="form-control" id="Procedure"/>
                       </div>
                       <ErrorMessage name ="Procedure"/>
                    </div>  

                    

                    <div className="form-group row">
                       <label htmlFor="Note"
                        className="offset-md-1 col-md-3 col-form-label">
                        Note :</label>
                       <div className="col-md-6">
                          <Field name="note" 
                          className="form-control"
                          component="textarea"
                          value="consectetur adipisicing elit Quod deserunt quo labore"
                          rows="6" id="note"/>
                       </div>
                       <ErrorMessage name ="note"/>
                    </div>                  
                                      
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            NoteDate: Yup.string().required(),
            Procedure: Yup.string().required(),
            note: Yup.string().required(),
            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{PatientName:"",time:"",
                    date: "", note: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
