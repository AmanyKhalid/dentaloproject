import React,{Component} from 'react';
import ItemList from './ListItems.js';
import AppointmentList from '../Api/Appointment.json';
import Model from './model.js';
import AddOppointment from './addOppointment.js';
import getAll from '../Api/getAll.js';
import $ from 'jquery';


export default class Appointment extends Component{

  state ={
    Appointments:[]
  }
  
  
    componentDidMount(){
	     getAll.getAll(AppointmentList)
	     .then(data=>{
	      this.setState({
	        Appointments:data
	      })
	      console.log(data);
	     });
	     $("#addOppointment").click(function(){
	    	if ( $("#addPatientDiv").is( ":hidden" ) ) {
            $("#addPatientDiv").slideDown("slow");
        }else{
        	$("#addPatientDiv").hide("slow");
        }
        });
    }
	

	render(){
		return(
			<div className="ListItems">
				<ItemList 
					headerlist="Appointments List" 
					buttonlist="Add Appointment"
					dataTarget="addOppointment"
				    headerIcon="far fa-calendar-check"
			    />
			    <div id="addPatientDiv">
					<AddOppointment/>
			    </div>
			    				
				<div className="container ">
					<div className="row rowHeader">
						<div className="col-md-1">No.</div>
						<div className="col-md-2">Patient Name</div>
						<div className="col-md-2">Date</div>
						<div className="col-md-2">Time</div>
						<div className="col-md-2">Note</div>
						
					</div>
					<div className="allItems">
						{this.state.Appointments.map(Appointment =>
							<div className="row OneItem" key={Appointment.id}>	
								<div className="col-md-1">{Appointment.id}</div>
								<div className="col-md-2">{Appointment.PatientNo}</div>
								<div className="col-md-2">{Appointment.date}</div>
								<div className="col-md-2">{Appointment.time}</div>
								<div className="col-md-2">{Appointment.note}</div>
								<div className="editClass">
								    <button className="btn" 
								        data-toggle="modal"
							            data-target="#addOppointment2">
								        <i className="fas fa-edit"></i>
								    </button>
								    <Model 
									  ModelHeader="Edit Oppointment"
									  HeaderIcon="far fa-calendar-check"
									  ModelId="addOppointment2" 
									  ModelContent=<AddOppointment PatientName={Appointment.PatientNo} 
									  data={Appointment.date}
									   time={Appointment.time} note={Appointment.note} 
									  />
				                    />
								    <button className="btn"><i className="fas fa-trash-alt"></i></button>
								</div>										
							</div>
						)}
					</div>
				</div>
				<Model 
				    ModelId="addOppointment"
				    ModelHeader="Add Oppointment"
					HeaderIcon="far fa-calendar-check" 
					ModelContent=<AddOppointment/>
				/>
			</div>
		);
	}
}