import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class Patient extends Component {

   
    onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
    	const fn=<p> First Name</p>
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="SignUpContent">

	                <div className="fullInput">
	                    <i className="fas fa-user-alt"></i>
                        <Field name = "First"
	                      placeholder="First Name"
                          
                          className="Field"
                        /><br/>
	                    <ErrorMessage name = "First" />
	                </div>

	                <div className="fullInput">
	                    <i className="fas fa-user-alt"></i><Field name = "Last"
	                      placeholder="Last Name"
                          
                          className="Field"/><br/>
	                    <ErrorMessage name = "Last" />
	                </div>

                    <div className="fullInput">
	                    <i className="fas fa-envelope"></i><Field name = "email"
	                      placeholder="example@email.com"
                          
                          className="Field"/><br/>
	                    <ErrorMessage name = "email" />
                    </div>

                    <div className="fullInput">
	                    <i className="fas fa-lock"></i><Field name = "password"
	                       placeholder="password"
                           
                           className="Field"/><br/>
	                    <ErrorMessage name = "password" />
                    </div>

                    <div className="fullInput">
                        <i className="fas fa-lock"></i><Field name = "password2"
                           placeholder="Retype password"
                           
                           className="Field"/><br/>
                        <ErrorMessage name = "password2" />
                    </div>

	                <div className="btnPage">
		                <button type="submit">Sign Up</button>
		                <p>Already  Have Account  <a href="#">Sign In</a></p>
		            </div>
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            email: Yup.string().required(),
            First: Yup.string().required(),
            Last: Yup.string().required(),
            password: Yup.string().required(),
            password2: Yup.string().required(),  
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{firstName:"",lastName:"",
                    email: "", password: "",password_confirmation:""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
    