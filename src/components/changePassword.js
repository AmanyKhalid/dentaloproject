import React from 'react';

export default function ChangePass(argument) {
	return(
		<div className="ChangePass">
			<h3><i className="fas fa-lock-open"></i> Change Password</h3>
			<div className="container">
				<form>
					<div className="form-group row">
					   <label for="CurrentPassword" className="offset-md-2 col-md-3 col-form-label">Current Password </label>
					   <div className="col-md-4">
					      <input type="password" className="form-control" id="CurrentPassword"/>
					   </div>
					</div>
					<div className="form-group row">
					    <label for="NewPassword" className="offset-md-2 col-md-3 col-form-label">New Password </label>
					    <div className="col-md-4">
					      <input type="password" className="form-control" id="NewPassword"/>
					    </div>
				    </div>
					<div className="form-group row">
					    <label for="ConfirmPassword" className="offset-md-2 col-md-3 col-form-label">Confirm New Password </label>
					    <div className="col-md-4">
					      <input type="password" className="form-control" id="ConfirmPassword"/>
					    </div>
					</div>
					<button className="btn" type="onSubmit">Send</button>
			    </form>
			</div>
		</div>
	);
}