import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
import Model from './model.js';
import PatientDrugs from './patientDrugs';
export default class AddPrescription extends Component {
     onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
                       <label htmlFor="Date"
                        className="offset-md-1 col-md-4 col-form-label">
                         Date :</label>
                       <div className="col-md-6">
                          <Field name="Date" type="date" 
                          className="form-control" id="Date"/>
                       </div>
                       <ErrorMessage name ="Date"/>
                    </div>  

                    <div className="form-group row">
                       <label htmlFor="Drugsdescription"
                        className="offset-md-1 col-md-4 col-form-label">
                        Drugs description :  </label>
                       <div className="col-md-6">
                          <Field name="Drugsdescription"
                          className="form-control" id="Drugsdescription"/>
                       </div>
                       <ErrorMessage name ="Drugsdescription"/>
                    </div>

                                                         
                     <button className="btn" type="submit">Save</button>                  
              </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            Date: Yup.string().required(),
            Drugsdescription: Yup.string().required(),
            
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{PatientName:"",time:"",
                    date: "", note: ""}}
                     onSubmit={this.onSubmit}
                     render  ={this.form}
                     validationSchema = {this.schema()}
                    /> 
                     <button 
                      className="btn" 
                      data-toggle="modal" 
                      data-target="#patientDrugs">
                      Add Drugs
                    </button>
                    <Model 
                      ModelHeader="Add Drugs"
                      HeaderIcon="fas fa-pills"
                      ModelId="patientDrugs" 
                      ModelContent=<PatientDrugs/>
                    /> 
                </div>
            );
    }
}
