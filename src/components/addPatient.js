import React, {Component} from 'react';
import {Formik,Field,ErrorMessage,FieldArray} from 'formik';
import axios from "axios";
import * as Yup from 'yup';
export default class AddPatient extends Component {

   
    onSubmit = (values) => {
        console.log(values);
    }

    form = (props) => {
    	const fn=<p> First Name</p>
        return(
            <form onSubmit = {this.handleSubmit} > 
                <div className="patientInfo">
                    <div className="form-group row">
					   <label htmlFor="First"
					    className="offset-md-1 col-md-3 col-form-label">
					    First Name :</label>
					   <div className="col-md-6">
					      <Field name="First" 
					      className="form-control" id="First"/>
					   </div>
					   <ErrorMessage name = "First"/>
					</div>	
					<div className="form-group row">
					   <label htmlFor="Middle"
					    className="offset-md-1 col-md-3 col-form-label">
					    Middle Name : </label>
					   <div className="col-md-6">
					      <Field name="Middle" 
					      className="form-control" id="Middle"/>
					   </div>
					   <ErrorMessage name = "Middle"/>
					</div>  
					<div className="form-group row">
					   <label htmlFor="Last"
					    className="offset-md-1 col-md-3 col-form-label">
					    Last Name :</label>
					   <div className="col-md-6">
					      <Field name="Last" 
					      className="form-control" id="Last"/>
					   </div>
					   <ErrorMessage name = "Last"/>
					</div>
					<div className="form-group row">
					   <label htmlFor="BirthDay"
					    className="offset-md-1 col-md-3 col-form-label">
					    BirthDay :</label>
					   <div className="col-md-6">
					      <Field type="date" 
					      className="form-control" id="BirthDay"/>
					   </div>
					   <ErrorMessage name = "BirthDay"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Gender"
					    className="offset-md-1 col-md-3 col-form-label">
					    Gender : </label>
					   <div className="col-md-6">
					    <Field name="Gender"
	                      type ="radio" value = "Male" />
	                      Male <br/>
	                    <Field name="Gender"
                         type ="radio" value = "Female" />
                         Female <br/>
					   </div>
					   <ErrorMessage name = "Gender"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Contact"
					    className="offset-md-1 col-md-3 col-form-label">
					    Contact No : </label>
					   <div className="col-md-6">
					      <Field name="Contact" 
					      className="form-control" id="Contact"/>
					   </div>
					   <ErrorMessage name = "Contact"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Email"
					    className="offset-md-1 col-md-3 col-form-label">
					    Email : </label>
					   <div className="col-md-6">
					      <Field name="email" 
					      className="form-control" id="Email"/>
					   </div>
					   <ErrorMessage name = "email"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Address"
					    className="offset-md-1 col-md-3 col-form-label">
					    Address :</label>
					   <div className="col-md-6">
					      <Field name="Address" 
					      className="form-control" id="Address"/>
					   </div>
					   <ErrorMessage name = "Address"/>
					</div>

					<div className="form-group row">
					   <label htmlFor="Note"
					    className="offset-md-1 col-md-3 col-form-label">
					    Note :</label>
					   <div className="col-md-6">
					      <Field name="Note" 
					      className="form-control" id="Note"/>
					   </div>
					   <ErrorMessage name = "Note"/>
					</div>
		            <button className="btn btn-block" type="submit">Add Patient</button>
		          
                </div>
            </form>
        );
    }

    schema = () => {
        const schema = Yup.object().shape({
            email: Yup.string().required(),
            First: Yup.string().required(),
            Last: Yup.string().required(),
            Middle: Yup.string().required(),
            Contact: Yup.string().required(),
            BirthDay: Yup.string().required(),
            Address: Yup.string().required(),
            Note: Yup.string().required(),
            Gender: Yup.string().required(),  
        });
        return schema;
    }

    render(){
            return ( 
                <div className = "formik" >
                   <Formik initialValues ={{First:this.props.firstName,
	                   	Last:this.props.lastName,email:this.props.email,
	                   	Middle:this.props.MiddleName,Contact:this.props.Contact,
	                   	BirthDay:this.props.BirthDay,Address:this.props.Address,
	                   	Note:this.props.Note,Gender:this.props.Gender}}
	                    onSubmit={this.onSubmit}
	                    render  ={this.form}
	                    validationSchema = {this.schema()}
                    /> 
                < /div>
            );
    }
}
