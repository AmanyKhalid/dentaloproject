	import Dentists from './dentist.json';
	export  function getAll(itemList) {
		return Promise.resolve(itemList);
	}

	export  function getById(id) {
		 const dentist =Dentists.find(item => item.id === id);
		 return Promise.resolve(dentist);
	}
	export default {
		getAll,
		getById
	}  