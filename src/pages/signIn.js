import React from 'react';
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';
import Header from '../components/header.js';
import HomePage from '../components/signin.js';
import ServicesPage from '../components/Services.js';
import AboutUs from '../components/aboutUs.js';
import ConectUs from '../components/conectUs.js';

import MainFooter from '../components/mainFooter.js';
import Logo from '../components/logo.js';
import '../css/signin.css';
export default function SignIn() {
 return(
		
		<BrowserRouter>
		<div className="main">
			<nav className="navbar navbar-expand-lg row ">
				  <a className="navbar-brand  col-md-3" href="#"><Logo /></a>
				  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span className="navbar-toggler-icon"><i className="fas fa-list-ul"></i></span>
				  </button>
				 <div className="collapse navbar-collapse container" id="navbarNav">
				    <ul className="navbar-nav offset-md-1 col-md-5">
				      <li className="nav-item active">
				        <NavLink className="nav-link" to="/">
				        Home <span className="sr-only">(current)</span></NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link" to="/Services">Services</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link" to="/About">About Us</NavLink>
				      </li>
				      <li className="nav-item">
				        <NavLink className="nav-link" to="/ConectUs">Conect Us</NavLink>
				      </li>

				      
				    </ul>
				    <div className=" col-md-3">
					    <button className="btn">Sing In</button>
					    <button className="btn">Sing up</button>
				    </div>
				 
				</div>
			</nav>
			<div className=" mainImag">
			    <div className="container"> 
					<div className="row">
						<div className="col-md-6 SignInMain">
							<Route path="/" exact component={HomePage} />
							<Route path="/Services" exact component={ServicesPage} />
							<Route path="/About" exact component={AboutUs} />
							<Route path="/ConectUs" exact component={ConectUs} />
						</div>
						<div className="offset-md-1 col-md-5">
							 <img src="./image/dent.png"/>
						</div>
					</div>
			    </div>
			    <footer>
				    <MainFooter />
			    </footer>
			</div>
		</div>
		</BrowserRouter>	
	);
}