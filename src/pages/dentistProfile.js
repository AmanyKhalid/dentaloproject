import React, { Component } from "react";
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';
import Patient from '../components/Patient.js';
import SignIn from '../components/sigInNav.js';
import ProfileHeader from '../components/profileHeader.js';
import HomeProfile from '../components/HomeProfile.js';
import EditProfile from '../components/editProfile.js';
import DrugsList from '../components/drugsList.js';
import MyAppointments from '../components/myAppointments.js';
import MyPatient from '../components/myPatient.js';
import ChangePassword from '../components/changePassword.js';
import ProcedureList from '../components/ProcedureList.js';
import AddPatient from '../components/addPatient.js';

import MainFooter from '../components/mainFooter.js';


import '../css/dentistProfile.css';

export default class DentistProfile extends Component {

    render(){
		return(
			<div className="homeDentist">
			<header style={{width:"100"+"%"}}>
				<ProfileHeader/>
			</header>
				<BrowserRouter>
					<div className="row">
						<div className="col-md-3 linksSide ">
							<div className="edit">
							    <Link className="nav-link" to="/dentistProfile">
								    <img src="./image/doctor.jpg" alt="doctor"/><br />
							    </Link>																
							</div>
							<div>
								<ul>
									<li><i className="fas fa-clinic-medical"></i>
									 My Clinic
										<ul>
										    <li>
										      <Link  to="/dentistProfile/profile"><i className="fas fa-user-md"></i> 
										       Profile
										      </Link>
										    </li>
										    <li>
										      <Link  to="/dentistProfile/drugsList"><i className="fas fa-pills"></i>
										       Drugs List
										      </Link>
										    </li>
										    <li>
											    <Link  to="/dentistProfile/procedureList"><i className="fas fa-tooth"></i>
											       Procedure List
											    </Link>
										    </li>
										    <li>
										      <Link  to="/dentistProfile/myAppointments"><i className="far fa-calendar-check"></i>
										        My Appointments</Link>
										    </li>
										    <li><Link  to="/dentistProfile/myPatient"><i className="fas fa-user-injured"></i>
										        My Patient</Link>
										    </li>
										</ul>
									</li>
									<li><i className="fas fa-cogs"></i>Setting
										<ul>
											<li>
										    <Link  to="/dentistProfile/changePassword"><i className="fas fa-lock-open"></i>Change Password</Link>
										    </li>
										</ul>
									</li>
								</ul>
		                    </div>
						</div>

					    <div className="col-md-9">
					    <Switch>
					   
					      <Route path="/dentistProfile" exact component={HomeProfile} />
					      <Route path="/dentistProfile/profile" exact component={EditProfile} />
					      <Route path="/dentistProfile/drugsList" exact component={DrugsList} />
					      <Route path="/dentistProfile/procedureList" exact component={ProcedureList} />
					      <Route path="/dentistProfile/myAppointments" exact component={MyAppointments} />
					      <Route path="/dentistProfile/myPatient" exact component={MyPatient} />
					      <Route path="/addPaient" exact component={AddPatient} />
					      <Route path="/dentistProfile/changePassword" exact component={ChangePassword} />					      
					    </Switch>
					    </div>
					</div>
                </BrowserRouter>
                <footer>
	                <MainFooter />
                </footer>
            </div>
		);
	}
}