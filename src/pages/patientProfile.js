import React from 'react';
import {BrowserRouter,Route,Link,NavLink,Switch} from 'react-router-dom';
import ProfileHeader from '../components/profileHeader.js';
import MainFooter from '../components/mainFooter.js';
import FadeIn from 'react-fade-in';
import Model from '../components/model.js';
import PatientHome from '../components/patientHome.js';
import DentistList from '../components/dentistList.js';

import '../css/patientProfile.css';


export default class PatientProfile extends React.Component{

	render(){

		return(
		    <BrowserRouter>
			<div className="MainPatient">
			<ProfileHeader/>
			<div className="row mainRow">
				<div className="col-md-3 patientInfo">
					<div className="infoCard">
					    <div className="imgClass">
							<NavLink className="nav-link" to="/PatientHome">
								<img src="../image/newsitem_news_1726_630.jpg"alt="patientImg"/>
							</NavLink>
							<NavLink className="nav-link" to="/PatientProfile">
								<h4>WELCOME </h4>
							</NavLink>
						</div>
						
						<p>Khalid Ahmed <br/>
					       25 years <br/>
						   Gaza - Alnaser<br/>
						   0599747515
						</p>
					</div>
					<div>
						<h5>Up-coming Appointment</h5>
						<div className="Upcoming">
							1st.Jan.2020   |  10:00am
						</div>
					</div>
				</div>
				<div className="col-md-9">
					 <Route
			              exact
			              path={"/patientProfile"}
			              render={props => (
			                <DentistList/>
                        )}
                    />
					<Route path="/PatientHome" exact component={PatientHome} />
			    </div>
		    </div>
		    <footer>
                <MainFooter />
            </footer>
		</div>
		</BrowserRouter>
	);
}
}








