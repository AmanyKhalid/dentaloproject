import React ,{ Component }  from 'react';
import logo from './logo.svg';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import axios from "axios";
import './App.css';
import SigIn from './pages/signIn.js';
import DentistProfile from './pages/dentistProfile.js';
import PatientProfile from './pages/patientProfile.js';



export default class  App extends Component {
 

    render(){
	  return (
	    <div className="App">
	      <BrowserRouter>
          <Switch>
            <Route
              exact
              path={"/"}
              render={props => (
                <SigIn/>
              )}
            />
            <Route
              exact
              path={"/dentistProfile"}
              render={props => (
                <DentistProfile/>
              )}
            /> 
            <Route
              exact
              path={"/patientProfile"}
              render={props => (
                <PatientProfile/>
              )}
            />
          </Switch>
        </BrowserRouter>

	    </div>
	  );
  }
}


